Command to execute

java -Xmx2G -Xms2G -cp ReltioEntitiesReader.jar com.entity.json.writer.JSONWriter <property_file>

java -jar ReltioEntitiesReader-4.1.jar <property_file> 

Example Properties in property file

API_URL=https://361-dataload2.reltio.com/reltio/api/SINQzOKkd5dJwS5
AUTH_URL=https://auth.reltio.com/oauth/token
USERNAME=
PASSWORD=
URI_FILE_PATH=/Users/lokesh/SanofiScripts/inputFiles/uri_HCP_PROD_P1_0122.txt
OUTPUT_FILE_PATH=/Users/lokesh/SanofiScripts/Loki_Script_Output/uri_HCP_PROD_P1_0122_extractResult.txt
RECORDS_PER_THREAD=150
THREADS=30