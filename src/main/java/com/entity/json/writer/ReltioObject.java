package com.entity.json.writer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Ganesh.Palanisamy@reltio.com Created : Sep 19, 2014
 */
public class ReltioObject {

	public String type;
	public String uri;
	public String label;
	public String secondaryLabel;
	public String createdBy;
	public String updatedBy;
	public Long createdTime;
	public Long updatedTime;
	public Map<String, List<Object>> attributes = new HashMap<String, List<Object>>();
	public List<Map<String, Object>> crosswalks;

	public Boolean isFavorite;

	public Object analyticsAttributes;
}
