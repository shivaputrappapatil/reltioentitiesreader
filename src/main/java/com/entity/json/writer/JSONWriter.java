package com.entity.json.writer;

/**
 * Created by lokesh on 2/13/17.
 */


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;
import com.google.gson.reflect.TypeToken;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.TokenGeneratorService;
import com.reltio.cst.service.impl.SimpleReltioAPIServiceImpl;
import com.reltio.cst.service.impl.TokenGeneratorServiceImpl;
import com.reltio.file.ReltioFileWriter;
import com.reltio.file.ReltioFlatFileWriter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class JSONWriter {
    public static Gson gson;


    public static final String BYURIS_API_INPUT = "{ \"uris\": uriListString  }";

    public static int numberOfURisProcessed = 0;

    static {
        GsonBuilder gsonBuilder = new GsonBuilder();
        // gsonBuilder.setLongSerializationPolicy( LongSerializationPolicy.STRING );
        // gsonBuilder.disableInnerClassSerialization();
        JSONWriter.gson = gsonBuilder.create();
    }

    public static void main(final String[] args) {
        try {
            new JSONWriter().startWriting(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long waitForTasksReady(final Collection<Future<Long>> futures, final int maxNumberInList) {
        long totalResult = 0L;
        while (futures.size() > maxNumberInList) {
            try {
                Thread.sleep(20L);
            } catch (Exception ex) {
            }
            final ArrayList<Future<Long>> ft = new ArrayList<Future<Long>>(futures);
            for (final Future<Long> future : ft) {
                if (future.isDone()) {
                    try {
                        totalResult += future.get();
                        futures.remove(future);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return totalResult;
    }

    public void startWriting(final String[] args) throws Exception {
        final Properties properties = new Properties();
        final long programStartTime = System.currentTimeMillis();
        try {
            final String propertyFilePath = args[0];
            final FileReader fileReader = new FileReader(propertyFilePath);
            properties.load(fileReader);
        } catch (Exception e) {
            System.out.println("Failed to read the Properties File :: ");
            e.printStackTrace();
        }
        final String inputFilePath = properties.getProperty("URI_FILE_PATH");
        final String outputFilePath = properties.getProperty("OUTPUT_FILE_PATH");
        final String apiURL = properties.getProperty("API_URL");
        final String username = properties.getProperty("USERNAME");
        final String pass = properties.getProperty("PASSWORD");
        final String authURL = properties.getProperty("AUTH_URL");
        final Integer threads = Integer.parseInt(properties.getProperty("THREADS"));
        final Integer recordsPerThread = Integer.parseInt(properties.getProperty("RECORDS_PER_THREAD"));
        BufferedReader inputFileReader = null;
        try {
            inputFileReader = new BufferedReader(new FileReader(inputFilePath));
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            System.out.println("File Not found. Exiting program !");
        }
        //final ArrayList<String> uriList = new ArrayList<String>();
        String line;
        //while ((line = inputFileReader.readLine()) != null) {
        //uriList.add(line);
        //}
        final TokenGeneratorService tokenGeneratorService = (TokenGeneratorService) new TokenGeneratorServiceImpl(username, pass, authURL);
        final ReltioAPIService reltioAPIService = (ReltioAPIService) new SimpleReltioAPIServiceImpl(tokenGeneratorService);
        final ReltioFileWriter reltioFile = (ReltioFileWriter) new ReltioFlatFileWriter(outputFilePath);
        final ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(threads);
        boolean eof = false;
        final ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();
        final StringBuffer[] entityURIGroup = new StringBuffer[(int) threads];
        Integer count = 0;
        StringBuffer requestJson = null;

        while (!eof) {
            for (int threadNum = 0; threadNum < threads; ++threadNum) {
                requestJson = new StringBuffer();
                for (int records = 0; records < recordsPerThread; records++) {
                    final String nextHcp = inputFileReader.readLine();
                    if (nextHcp == null) {
                        eof = true;
                        break;
                    }
                    //final ArrayList<String> list = uriList;
                    //final Integer n = count;
                    //count = n + 1;
                    //final String nextHcp = list.get(n);
                    if (nextHcp.length() >= 1) {
                        if (requestJson.length() > 1) {
                            requestJson.append('|');
                        }
                        requestJson.append(nextHcp);
                    }
                }

                if (requestJson.length() > 1) {

                    final String entityURIGroupString = requestJson.toString();
                    final Callable<Long> callable = new Callable<Long>() {
                        @Override
                        public Long call() throws Exception {
                            long requestExecutionTime = 0L;
                            final long startTime = System.currentTimeMillis();
                            final String[] entityURIS = entityURIGroupString.split("\\|");
                            String urisReqestBody = gson.toJson(entityURIS);
                            String[] array;
                            String apiInputBody = BYURIS_API_INPUT.replaceAll("uriListString", urisReqestBody.toString());
                            //for (int length = (array = entityURIS).length, i = 0; i < length; ++i) {
                            //final String entityURI = array[i];
                            int trial = 0;
                            while (true) {
                                try {
                                    //final String scanResponse = reltioAPIService.get(String.valueOf(apiURL) + "/" + entityURI);
                                    final String scanResponse = reltioAPIService.post(String.valueOf(apiURL) + "/entities/_byUris", apiInputBody);
                                    //  reltioFile.writeToFile(scanResponse);

                                    List<ReltioObject> entitiesList = gson.fromJson(scanResponse, new TypeToken<List<ReltioObject>>() {
                                    }.getType());

                                    for (ReltioObject entity : entitiesList) {
                                        reltioFile.writeToFile(gson.toJson(entity));
                                    }

                                    long elapsedTime = System.currentTimeMillis() - programStartTime;
                                    synchronized (this) {
                                        System.out.println("PASS|" + urisReqestBody);
                                        numberOfURisProcessed = numberOfURisProcessed + entityURIS.length;
                                        System.out.println(numberOfURisProcessed + " entity uris processed in " + elapsedTime / 1000L + " seconds");
                                    }
                                } catch (Exception e) {
                                    System.out.println("WARN|" + trial + " exception: " + e.getMessage());
                                    Thread.sleep(50L);
                                    if (++trial == 3) {
                                        synchronized (this) {
                                            System.out.println("ERROR|" + urisReqestBody);
                                        }
                                    }
                                    if (trial < 3) {
                                        continue;
                                    }
                                }
                                break;
                            }
                            //}
                            requestExecutionTime = System.currentTimeMillis() - startTime;
                            return requestExecutionTime;
                        }
                    };

                    futures.add(executorService.submit(callable));
                }

                if (eof) {
                    break;
                }
            }
            waitForTasksReady(futures, threads * 5);

            if (eof) {
                break;
            }
        }
        waitForTasksReady(futures, 0);
        executorService.shutdown();
        reltioFile.close();
        System.out.println("Completed..... ");
        final long finalTime = System.currentTimeMillis() - programStartTime;
        System.out.println("[Performance]:  Total processing time : " + finalTime / 1000L + "  Seconds");
    }
}
